<?php
session_start();

require("databaseaccess.php");

//CSRF Check:
if (isset($_POST['token'])) {
  if($_SESSION['token'] !== $_POST['token']){
    die("Request forgery detected");
  }
}
//grab storyID if an edit no a new story
if(isset($_POST['storyID'])){
  $storyid=$_POST['storyID'];
}

$user_id = $_SESSION['user_id'];
if (isset($_POST['storyURL'])) {
  $storyURL = $_POST['storyURL'];

  if (isset($_POST['textarea'])){
    $storyComment= $_POST['textarea'];
  }
  else {$storyComment = ' ';}

  if (isset($_POST['storyTitle'])){
    $storyTitle= $_POST['storyTitle'];
  }
  else {$storyTitle = ' ';}
  
  //place  Story into database
  if(!isset($_POST['storyID'])){
    $stmt = $mysqli->prepare("insert into story (userid, url, text, title) values (?, ?, ?, ?)");

    if(!$stmt){
      printf("Query Prep Failed: %s\n", $mysqli->error);
      exit;
    }

    $stmt->bind_param('isss', $user_id, $storyURL, $storyComment, $storyTitle);
  }else{
    $stmt = $mysqli->prepare("UPDATE story SET url=?,title=?,text=? WHERE storyID=?");
    if(!$stmt){
      printf("Query Prep Failed: %s\n", $mysqli->error);
      exit;
    }
    $stmt->bind_param('sssi',  $storyURL, $storyTitle, $storyComment, $storyid);

  }
  if(!$stmt->execute()){
    print("failure");

  }
  $stmt->close();

  //So now we've added the story to the database. Let's redirect back to main page
  header("Location: mainpage.php");


}
?>

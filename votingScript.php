<?php

    require("databaseaccess.php");
    session_start();

    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }
    
    $userid = $_SESSION['user_id'];
    if ($_POST['vote'] == 0) {
        header('Location: mainpage.php');
        exit;
    }
    if ($_POST['vote'] == 1) {
        $scoreChange = 1;
    }
    else $scoreChange = -1;
    
    //check tosee if the user has voted
    $storyid = $_POST['storyID'];
    $stmt = $mysqli->prepare("insert into userStoryCross (storyid, userid, vote) value (?,?,?)");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('iii', $storyid ,$userid, $scoreChange);
    if(!$stmt->execute()){
        print("You already voted!");
        echo ("<a href=mainpage.php>Go Back.</a>");
        exit;
    }
    $stmt->close();
    
    //Grab the current score
    $stmt = $mysqli->prepare("SELECT score FROM story WHERE storyid=?");
    if(!$stmt){
      printf("Prepping GrabScore Failed: %s\n", $mysqli->error);
      exit;
    }
    $stmt->bind_param('i',$storyid); //changed this to int, unless you think it should be string
    $stmt->execute();
    $stmt->bind_result($currentScore);
    $stmt->close();
    
    
    //update score, then send them back
    $newScore = $currentScore + $scoreChange;

    $stmt = $mysqli->prepare("UPDATE story SET score=? WHERE storyid=?");
    $stmt->bind_param('ii',$newScore,$storyid);
    if(!$stmt){
        printf("Prepping Score Update Failed: %s\n", $mysqli->error);
        exit;
    }   
    if(!$stmt->execute()){
        printf("Executing score update failed: %s\n", $mysqli->error);
    }
    $stmt->close();
    header('Location: mainpage.php');
?>

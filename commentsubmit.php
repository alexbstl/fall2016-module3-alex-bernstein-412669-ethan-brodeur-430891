<?php
require("databaseaccess.php");
session_start();
$storyid = $_SESSION['storyid'];
$location = "storyPage.php?storyID=".$storyid;
if (isset($_POST['token'])){
  if($_SESSION['token'] !== $_POST['token']){
    echo $_POST['token'];
    var_dump($_SESSION);
    die("Request forgery detected");
  }
}
if (isset($_POST['textarea'])) {

  //GRAB THE COMMENTING USER's SCRENNAME
  $userID = $_SESSION['user_id'];
  $stmt = $mysqli->prepare("SELECT screenname FROM users where userID=?");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }
  $stmt->bind_param('i',$userID); //changed this to an int, let me know if you think it should still be string
  $stmt->execute();
  $stmt->bind_result($screenname);
  $stmt->fetch();
  $stmt->close();
  //GRAB THE COMMENT AND INSERT

  $text=$_POST['textarea']; //does the prepare statement below protect us from injections here?
  $commentID=$_POST['commentID'];
  if($commentID<0){
  $stmt = $mysqli->prepare("insert into comments (userID, commentText, userscreenname, storyID) values (?, ?, ?, ?)");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }

  $stmt->bind_param('issi', $userID, $text, $screenname, $storyid);

  if(!$stmt->execute()){
    printf("Query Execute Failed: %s\n", $mysqli->error);
  }

  $stmt->close();
}else{
  $stmt = $mysqli->prepare("UPDATE comments SET commentText=? WHERE commentID=?");
  if(!$stmt){
      printf("Query Prep Failed: %s\n", $mysqli->error);
      exit;
  }
  $stmt->bind_param('si', $text, $commentID);

  if(!$stmt->execute()){
      print("failure");
  }
  $stmt->close();
}
  //Refresh page
  header('Location:'.$location);
}
?>

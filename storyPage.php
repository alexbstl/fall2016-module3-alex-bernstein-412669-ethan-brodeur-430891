<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="redditcss.css" />
  <meta charset="utf-8"/>
  <title>Comments Page</title>
</head>
<body>
  <a href="mainpage.php">Go back to main page</a> <br>

  <?php

  session_start();
  require("databaseaccess.php");


  $_SESSION['storyid'] = $_GET['storyID'];
  $storyid = $_SESSION['storyid'];
  //DISPLAY STORY INFORMATION
  $stmt = $mysqli->prepare("select userid, url, text, score, submittime, title from story where storyID=?");

  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }
  $stmt->bind_param('i',$storyid); //changed this to int, unless you think it should be string
  $stmt->execute();
  $stmt->bind_result($storyuserid, $storyURL, $storyComment, $score, $submittime, $title);
  $stmt->fetch();
  $stmt->close();
  //Almost done grabbing info about the story. We just need the submitting user's screen name.

  $stmt = $mysqli->prepare("select screenname FROM users where userid=?");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }
  $stmt->bind_param('i',$storyuserid); //might need to be 's' here
  $stmt->execute();
  $stmt->bind_result($storyusername);
  $stmt->fetch();
  $stmt->close();

  //Print out all the story info
  echo "<header>";
  echo 'Submitting User: '.'<div>'.htmlentities($storyusername).'</div>';
  echo 'Link to story: '.'<div><a href=http://`'.htmlentities($storyURL).'>'.$title.'</a></div>'; //Link to story URL
  echo 'Score: '.'<div>'.htmlentities($score).'</div>';
  echo 'Time Posted: '.'<div>'.htmlentities($submittime).'</div>';
  echo 'Story Description: '.'<div>'.htmlentities($storyComment).'</div>';
  echo "</header>";

  //DISPLAY COMMENT INFORMATION
  $stmt = $mysqli->prepare("select userID, commentText, userscreenname, commentID from comments where storyID=?");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }
  $stmt->bind_param('i',$storyid); //changed this to int, unless you think it should be string
  $stmt->execute();

  $stmt->bind_result($userid, $text, $user, $commentID);
  echo "<ul>\n";

  while($stmt->fetch()){
    print("<li>");
    printf("\t %s %s \n",
    htmlentities($user),
    htmlentities($text)
  );
  //If comment submitter is current user, allow edit or delete
  if($userid==$_SESSION['user_id']){
    print("<form method='POST' action='editdelete.php'>");
    printf("<input type='hidden' name='token' value=%s />",$_SESSION['token']);
    printf("<input type='hidden' name='ID' value=%s />",$commentID);
    printf("<input type='hidden' name='storyID' value=%s />",$storyid);
    print("<input type='hidden' name='edtype' value=1 />");
    print("<input type='submit' value='Edit' name='action'>");
    print("<input type='submit' value='Delete' name='action'> <br>");
    print("</form>");
  }
  print("</li>");
}
echo "</ul>\n";
?>
<br>
<form method="POST" action="commentsubmit.php">
  <p>
    <label>Comments?</label><br>
    <textarea name="textarea" style="width:450px;height:150px;"></textarea>
    <input type="submit" value="submit" /> <br>
    <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
    <input type='hidden' name='commentID' value='-1' />
  </p>
</form>


</body>
</html>

<?php
include("databaseaccess.php");
session_start();
if (isset($_POST['token'])){
  if($_SESSION['token'] !== $_POST['token']){
    die("Request forgery detected");
  }
}

if ( !preg_match('/^[\w_\-]+$/', $_POST['username']) ){
        echo "Invalid username. Please go back and try an alphanumeric username.";
        exit;
        }
if ($_POST['username']=="deletedUser"){
        echo "You can't use that username. Please go back and try again.";
        exit;
}

$stmt = $mysqli->prepare("SELECT COUNT(*), userid, password FROM users where screenname=?");
$stmt->bind_param('s',$user);
$user = $_POST['username'];
$stmt->execute();

$stmt->bind_result($cnt, $userid, $pwd_hash);
$stmt->fetch();

$pwd_guess = $_POST['password'];
// Compare the submitted password to the actual password hash
if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
  // Login succeeded!
  $_SESSION['user_id'] = $userid;
  $_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string, for CSRF Safety
  $stmt->close();
  header('Location: mainpage.php');
}else{
  $stmt->close();
  header('Location: login.php');
}
?>

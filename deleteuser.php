<?php
require("databaseaccess.php");
session_start();
$deleteID=-5;
$user_id = $_SESSION['user_id'];

$stmt=$mysqli->prepare("select screenname from users where userid=?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
$stmt->bind_param('i',$deleteID);
if(!$stmt->execute()){
print("failure");
}
$stmt->bind_result($deletedName);
$stmt->fetch();
$stmt->close();


//change all this user's stories and comments to say "deletedUser"
$stmt = $mysqli->prepare("UPDATE story SET userid=? WHERE userid=?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
$stmt->bind_param('ii',$deleteID,$user_id);
if(!$stmt->execute()){
print("failure");
}
$stmt->close();

$stmt = $mysqli->prepare("UPDATE comments SET userID=?, userscreenname=? WHERE userID=?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
$stmt->bind_param('isi',$deleteID,$deletedName,$user_id);
if(!$stmt->execute()){
print("failure");
}
$stmt->close();

//deleted users votes are not subtracted but records are removed
$stmt = $mysqli->prepare("delete from userStoryCross WHERE userid=?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
$stmt->bind_param('i',$user_id);
if(!$stmt->execute()){
print("failure");
}
$stmt->close();

//delete user
$stmt = $mysqli->prepare("delete from users WHERE userid=?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
$stmt->bind_param('i',$user_id);
if(!$stmt->execute()){
print("failure");
}
$stmt->close();

header("Location: logout.php");
?>

<html>
<head>
</head>
<body>
<?php
require("databaseaccess.php");
session_start();

if (isset($_POST['token'])) {
    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }
}

if (isset($_POST['ID'])){
  $queryID = $_POST['ID'];
  $storyid = $_POST['storyID'];
if($_POST['edtype']==1){
  $querytable="comments";
  $querystuff="commentText";
  $queryIDtype="commentID";
  $location = "storyPage.php?storyID=".$storyid;
}else if($_POST['edtype']==2){
  $querytable="story";
  $querystuff="url, text, title";
  $queryIDtype="storyid";
  $location="mainpage.php";
}else{
  $location="mainpage.php";
  header('Location:'.$location);
}
}
if ($_POST['action']=='Edit'){
  $_SESSION['token'] = substr(md5(rand()), 0, 10);

  $queryval = "select ".$querystuff." from ".$querytable." where ".$queryIDtype."=?";
  $stmt = $mysqli->prepare($queryval);
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }
  $stmt->bind_param('i',$queryID); //changed this to int, unless you think it should be string
  $stmt->execute();
if($_POST['edtype']==1){
  $stmt->bind_result($text);
  $stmt->fetch();
  $stmt->close();
  print("<form method='POST' action='commentsubmit.php'>");
  printf("<input type='hidden' name='token' value=%s >",$_SESSION['token']);
  printf("<input type='hidden' name='commentID' value=%s />",$queryID);
  print("<p>");
  printf("<textarea name='textarea' style='width:450px;height:150px;'>%s</textarea>",$text);
  print("<input type='submit' value='submit' /> <br> </p>  </form>");
}else{
  $stmt->bind_result($url,$text, $title);
  $stmt->fetch();
  $stmt->close();
  print("<form method='POST' action='submissionScript.php'>");
  printf("<input type='hidden' name='token' value=%s>",$_SESSION['token']);
  printf("<input type='hidden' name='storyID' value=%s />",$storyid);
  print("<p>");
  print("<label for 'storyURL'>Story URL:</label>");
  printf("<input type='text' name='storyURL'required id='storyURL' value=%s /><br>",$url);
  print("<label for 'storyTitle'>Story Title:</label>");
  printf("<input type='text' name='storyTitle'required id='storyTitle' value=%s /><br>",$title);
  printf("<textarea name='textarea' style='width:450px;height:150px;'>%s</textarea>",$text);
  print("<input type='submit' value='submit' /> <br> </p>  </form>");
}
}else if($_POST['action']=='Delete'){
  //must delete all comments if we want to delete a story
  if($_POST['edtype']==2){
    $stmt = $mysqli->prepare("delete from comments where storyID=?");
    $stmt->bind_param('i',$storyid);
    $stmt->execute();
    $stmt->close();
  }
  $queryval="delete from ".$querytable." where ".$queryIDtype."=?";
  $stmt = $mysqli->prepare($queryval);
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }
  $stmt->bind_param('i',$queryID); //changed this to int, unless you think it should be string
  $stmt->execute();
  $stmt->close();
  header('Location:'.$location);

}else{
  header('Location:'.$location);
}
?>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <title>Submit a Story</title>
  <link rel="stylesheet" type="text/css" href="redditcss.css" />
</head>

<?php
session_start();
?>


<body>
  <form method="POST" action="submissionScript.php">
    <p>
      <label for="storyURL">Story URL:</label>
      <input type="text" name="storyURL" autofocus required id="storyURL"/> <br><br>
      <label for="storyTitle">Title of the Story:</label>
      <input type="text" name="storyTitle" required id="storyTitle"/>
      <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
      <label>Story Comments?</label><br>
      <textarea name="textarea" style="width:450px;height:150px;"></textarea>
      <input type="submit" value="submit" /> <br>

    </p>
  </form>
</body>




</html>

<!DOCTYPE html>
<?php
include("databaseaccess.php");
session_start();
$_SESSION['token'] = substr(md5(rand()), 0, 10);
?>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="redditcss.css" />
  <title> Login </title>
  <meta charset="utf-8">
</head>
<body>
  <h2>
    Enter a Username and Password
  </h2>
  <form action="loginscript.php" method="post">
    <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
    <p>
      <label for="username">User: </label>
      <input type="text" name="username"><br>
      <label for="password" name="password">Password: </label>
        <input type="password" name="password"><br>
        <input type="submit">
      </p>
    </form>
    <h3><a href="registration.php">Create New User</a></h3>
</body>
</html>

<?php
include("databaseaccess.php");
session_start();
$email=$_POST['email'];
$username = $_POST['username'];
$pass_hash = crypt($_POST['password']) ;

if( !preg_match('/^[\w_\-]+$/', $username) ){
        echo "Invalid username. Please go back and try an alphanumeric username.";
        exit;
}

if (isset($_POST['token'])){
  if($_SESSION['token'] !== $_POST['token']){
    die("Request forgery detected");
  }
}

$emailq = $mysqli->prepare("SELECT COUNT(*) FROM users where email=?");
$emailq->bind_param('s',$email);
$nameq = $mysqli->prepare("SELECT COUNT(*) FROM users where screenname=?");
$nameq->bind_param('s',$username);

$emailq->execute();
$emailq->bind_result($emailcnt);
$emailq->fetch();
$emailq->close();

$nameq->execute();
$nameq->bind_result($namecnt);
$nameq->fetch();
$nameq->close();

if(!$emailcnt>0){
  if(!$namecnt>0){
    $createq=$mysqli->prepare("insert into users (email,screenname,password) VALUES (?,?,?)");
    if(!$createq){
      printf("Query Prep Failed: %s\n", $mysqli->error);
      exit;
    }
    $createq->bind_param('sss',$email,$username, $pass_hash);
    if($createq->execute()){
      $createq->close();
      $checkq = $mysqli->prepare("SELECT userid FROM users where email=?");
      $checkq->bind_param('s',$email);
      $checkq->execute();
      $checkq->bind_result($userid);
      $checkq->fetch();
      $_SESSION['user_id']=$userid;
      header('Location: mainpage.php');
     $checkq->close();
    }else{
      printf("Create User failed");
      print($createq->error);

    }
  }
  else{
    //name exists already but email doesn't
    printf("Username %s already exists.",$username);


  }
}else{
  //email already exists in the table
  print("That email already has an account");

}
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="redditcss.css" />
  <meta charset="utf-8"/>
  <title>View All Stories</title>
</head>
<body>
  <header>
    <p><a href="logout.php">Logout</a></p>
    <p><a href="submissionPage.php">Submit a Story</a></p>
    <p><a href="deleteuser.php">Delete Profile</a><p>
  </header>

  <?php
  //grab some session stuff
  require("databaseaccess.php");
  session_start();
  $user_id = $_SESSION['user_id'];

  //load the Story Table
  $stmt = $mysqli->prepare("SELECT storyid, userid, url, score, submittime, title FROM story ORDER BY score DESC");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }

  $stmt->execute();

  //BIND our SQL data to PHP variables. MAKE SURE NO VARIABLES (INCLUD. SESSION VARS) DO NOT SHARE THIS NAME
  $stmt->bind_result($sqlstoryid, $sqluserid, $sqlurl, $sqlscore, $sqlsubmittime, $sqltitle);
  echo "<ul>\n";



  //The next Loop prints out all of our stories with links to each
  while($stmt->fetch()){
    print("<li>");
    $storyPageURL = "storyPage.php?storyID=".$sqlstoryid; //SHOULD SEND A _GET variable to storyPage.php
    echo ("<form method='POST' action='votingScript.php'>
    <fieldset>
    <label for='upvote'>Upvote</label>
    <input type ='radio' name='vote' value='1' id='upvote'/>
    <label for='null'>No Vote</label>
    <input type = 'radio' name='vote' value='0' id='null'/>
    <label for='downvote'>Downvote</label>
    <input type ='radio' name='vote' value='-1' id='downvote'/>
    <input type='submit' value='submit'/>
    </fieldset>
    <input type='hidden' name='storyID' value=".$sqlstoryid."/>");
    printf("<input type='hidden' name='token' value=%s />",$_SESSION['token']);
    echo("</form>");
    $printScore = "Score: ".$sqlscore;
    printf("\t %s %s %s \n",
      "<a href=".$storyPageURL.">".$sqltitle."</a>",
      htmlspecialchars($printScore),
      htmlspecialchars($sqlsubmittime)
    );
    if($sqluserid==$_SESSION['user_id']){
      print("<form method='POST' action='editdelete.php'>");
      printf("<input type='hidden' name='token' value=%s />",$_SESSION['token']);
      printf("<input type='hidden' name='ID' value=%s />",$sqlstoryid);
      printf("<input type='hidden' name='storyID' value=%s />",$sqlstoryid);
      print("<input type='hidden' name='edtype' value=2 />");
      print("<input type='submit' value='Edit' name='action'>");
      print("<input type='submit' value='Delete' name='action'> <br>");
      print("</form>");
    }
    print("</li>");
  echo ("<br>");
}
echo "</ul>\n";

$stmt->close();

?>
<br><br><br>
</body>
</html>
